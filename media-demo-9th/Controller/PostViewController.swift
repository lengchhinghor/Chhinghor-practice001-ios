//
//  PostViewController.swift
//  media-demo-9th
//
//  Created by Chhinghor's MacBook Pro on 13/11/21.
//

import UIKit
import ProgressHUD
import Kingfisher

class PostViewController: UIViewController {

  
    @IBOutlet weak var CaptionLabel: UITextField!
//    @IBOutlet weak var CaptionLabel: UILabel!
    @IBOutlet weak var imagePost: UIImageView!
    
    // step 1
    var pickerView = UIImagePickerController()
    let alertCon = UIAlertController(title: "Choose Options", message: nil, preferredStyle: .actionSheet)

    var imageData: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //2 Set up Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showOptions))
        
        self.imagePost.isUserInteractionEnabled = true
        self.imagePost.addGestureRecognizer(tapGesture)
        pickerView.delegate = self
        prepareOptions()
        // Do any additional setup after loading the view.
    }

    func chooseOptions(option: UIImagePickerController.SourceType){
        self.pickerView.allowsEditing = true
        self.pickerView.mediaTypes = ["public.image"]
        self.pickerView.sourceType = option
        
        present(self.pickerView, animated: true, completion: nil)
      
    }
    
    @objc func showOptions(){
        present(alertCon, animated: true, completion: nil)
    }
    
    func prepareOptions(){
        let camera = UIAlertAction(title: "Camera", style: .default) { _ in
            self.chooseOptions(option: .camera)
        }
        let gallary = UIAlertAction(title: "Gallary", style: .default) { _ in
            self.chooseOptions(option: .photoLibrary)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertCon.addAction(camera)
        alertCon.addAction(gallary)
        alertCon.addAction(cancel)
    }
    
    @IBAction func buttonPressCreate(_ sender: Any) {
        
        let caption = CaptionLabel.text
        ProgressHUD.show()
        PostService.shared.uploadImage(imageData: imageData) { url in
            PostService.shared.createPost(caption: caption!, image: url ?? "" ){ result in
                switch result {
                case .success(let msg):
                    ProgressHUD.showSucceed(msg)
                case .failure(let error):
                    ProgressHUD.showError(error.localizedDescription)
                }
            }
        
    }
    
    
}
}

extension PostViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
     
        if let possibleImage = info[.editedImage] as? UIImage {
            self.imagePost.image = possibleImage

            self.imageData = possibleImage.jpegData(compressionQuality: 1.0)
            
        }
        
        dismiss(animated: true)
    }
}
